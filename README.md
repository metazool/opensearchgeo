# opensearchgeo

This is intended to be a sample implementation
of the Opensearch-Geo extensions for spatial data search. 

It searches over a mongodb store of GeoJSON documents,
can search for docs or individual features, and is 
written in clojure, in theory

## Installation

Get from https://bitbucket.org/metazool/opensearchgeo

To get the dependencies and see the core working, run tests

 $ lein spec 

From the opensearchgeo directory


## Usage

Creates a mongodb instance by default test/test.

Add a spatial index to the collection thusly:

> db.collection.ensureIndex( { extents : "2dsphere" } )


## API 

This implements the OpenSearch Geo Extensions
As documented here:

http://www.opengeospatial.org/standards/opensearchgeo


Extension to search for individual features as well as whole docs
because i am an arsehole troll!

## Examples

...

## License

BSD-style
