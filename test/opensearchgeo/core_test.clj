(ns opensearchgeo.core-test
  (:require [clojure.test :refer :all]
            [cheshire.core :as json]
            [opensearchgeo.core :refer :all]))


(def testfeatures { 
                    :features [
                    {:geometry {:coordinates [[[0.0 51.1]]]}}
                    {:geometry {:coordinates [[[0.2 51.3][0.4,51.2]]]}}
                    ]} )

(def pairs [[0.0 51.5][0.2 52.3][0.1 54.5]])

(deftest find-bbox-test
  (def geojson (json/parse-string (slurp "test/fixtures/simple.json") true))
  (testing (find-bbox geojson))

;  (testing (find-bbox testfeatures))
)

(deftest get-coords-test
  (testing
     (get-coords {:geometry {:coordinates [[[0.0 51.1]]]}})
  )
)

;(deftest find-longs-test
;  (testing
;    (let [f] [(first (sort (find-min-max pairs 1)))]
;  (find-min-max pairs 1 1 0)
; (is (= 51.5 f)))
;  )
;)

;(deftest find-coords-test 
;  (testing
;   (find-min-max (get-coords (get testfeatures 0)) 0 0 0)))
 
