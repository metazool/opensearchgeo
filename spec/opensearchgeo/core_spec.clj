(ns opensearchgeo.core-spec
  (:require [speclj.core :refer :all]
            [opensearchgeo.core :as osgeo]
            [cheshire.core :as json]))


(describe "find-bbox"

  (it "should return a map with keys :minx :miny :maxx :maxy"
      (let [bbox (osgeo/find-bbox (json/parse-string (slurp "test/fixtures/simple.json") true))]
        (println bbox)
        (should== [:minx :miny :maxx :maxy] (keys bbox)))))

