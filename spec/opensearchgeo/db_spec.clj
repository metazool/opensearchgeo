(ns opensearchgeo.db-spec
  (:require [speclj.core :refer :all]
            [opensearchgeo.db :as db]
            [cheshire.core :as json]))


(describe "insert-file"
  (it "should insert a file into mongodb test"
      (let [done (db/insert-file "test/fixtures/simple.json")]
        (println done)
        (should== [:done] (keys done)))))

(describe "find-within"
  (it "should return some documents in the bounding box"
     (let [done (db/find-within {:minx -3.72 :maxx -3.70 :miny 40.4 :maxy 40.5 })]
       (println done))))

