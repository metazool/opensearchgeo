(ns opensearchgeo.core
  (:gen-class))

(declare find-bbox find-centroid get-coords find-min-max map->extents)

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))


(defn find-bbox 
  "Expects a GeoJSON doc through cheshire's json/parse_string"
; USAGE:   (find-bbox (json/parse-string (slurp "example.json") true))  

  [doc] 

  (let [coordlist (map get-coords (doc :features))]  
  (def coords (reduce into coordlist))
  
  (def lons (map (fn [c] (get c 0)) coords))
  (def lats (map (fn [c] (get c 1)) coords))

  (def minx (first (sort lats)))
  (def miny (first (sort lons)))
  (def maxx (last (sort lats)))
  (def maxy (last (sort lons)))
  {:miny miny :minx minx :maxx maxx :maxy maxy}))
       

(defn get-coords 
  "docstring"
  [feature]
  (let [coords (get-in feature [:geometry :coordinates])]
;  (println "GOT" (count coords))
;  (println coords)
  (get coords 0)))

(defn extents 
   [doc]
   (def bbox (find-bbox doc))
   (map->extents bbox))

(defn map->extents
   "Turns a map of min/max/x/y params into coordinate extents"
   [bbox]
   (vector [[(bbox :minx) (bbox :miny)]
             [(bbox :minx) (bbox :maxy)]
             [(bbox :maxx) (bbox :maxy)]
             [(bbox :maxx) (bbox :miny)]
             [(bbox :minx) (bbox :miny)]]))
