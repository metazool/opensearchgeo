(ns opensearchgeo.service
(:require [opensearchgeo.core :as geo]
          [opensearchgeo.db :as db]
          [ring.middleware.params :as rp]
          [cheshire.core :as json]))


(defn search [{params :params}]
  (if (not (nil? (params "minx")))
   (do
    (let [docs (db/find-within {:minx (params "minx")
                           :maxx (params "maxx")
                           :miny (params "miny")
                           :maxy (params "maxy")})]

    {:status  200
     :headers {"Content-Type" "text/plain"}
     :body    (str "docs" docs)})))
   (do
    {:status 204 }))

(def app
  (rp/wrap-params search))
