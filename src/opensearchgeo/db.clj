(ns opensearchgeo.db

  (:require [opensearchgeo.core :as geo]
            [monger.core :as mg]
            [monger.collection :as mc]
            [monger.operators :refer :all]
            [monger.query :as mq]
            [cheshire.core :as json]))

; public interfaces
(declare insert-file)

(mg/connect!)
(mg/set-db! (mg/get-db "test"))
(def dbname "test")

(defn insert-file [file]
  (let [f (json/parse-string (slurp file) true)]
  (try
    (def extents (geo/extents f))
    ;(println extents)
    ;(assoc f :extents extents)
    ;(println f)
    (do (mc/insert dbname (assoc f :extents extents)))
      (catch Exception e false))
   {:done 1}))


(defn find-within
   "Find all documents within bounding box"
   [bbox]
   (def extents (geo/map->extents bbox))
   (println extents)
   (mc/find-maps dbname { :extents { "$geoWithin" { "$geometry" { :type "Polygon" :coordinates extents }}}}))  
   
;
;  
; db.<collection>.find( { <location field> :
;                         { $geoWithin :
;                            { $geometry :
;                               { type : "Polygon" ,
;                                 coordinates : [ [ [ <lng1>, <lat1> ] , [ <lng2>, <lat2> ] ... ] ]
;                      } } } } )
